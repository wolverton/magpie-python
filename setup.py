from setuptools import setup, find_packages
import os

with open('README.md') as f:
    readme = f.read()

with open('LICENSE') as f:
    license = f.read()

setup(
    name='magpiepy',
    version='0.0.1',
    description='Wrapper for REST interface of Magpie',
    long_description=readme,
    url='TBD',
    license=license,
    install_requires=[
        'pandas',
        'requests',
        'numpy'
    ],
    entry_points=dict(
        console_scripts=["magpie = magpie.cmd:run"]
    ),
    packages=find_packages(exclude=('tests', 'docs'))
)
