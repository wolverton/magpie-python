# MagpiePy
------

A Python wrapper for the REST API of [Magpie](https://bitbucket.org/wolverton/magpie)

Makes it possible to run machine learning models created using Magpie from a simple Python interface

## Installation

MagpiePy is not on PyPi, so you must clone the repository and then install it using:

```bash

pip install -e .
```

## Usage

The Magpie library installs a command line client, `magpie`, that will let you make simple requests to the server.

For example, you can predict the DFT formation enthalpy of NaCl by calling:

```bash
echo NaCl | magpie attributes oqmd-dH
```

Or, you can compute the features used in a model that predicts glass-forming ability using machine learning with:

```bash
echo NaCl | magpie --output attributes.csv --format csv attributes gfa
```

For a complete listing of the available models, calling

```bash
magpie models property units description citation
```

## Advanced Usage

Beyond the command line interface, you can use MagpiePy via its Python API. 
You must first create a `MagpieServer` object that establishes a connection to the server running Magpie.
That `MagpieServer` class possesses several useful wrappers for calling commands. 
As an example, computing the DFT formation enthalpy of NaCl can be accomplished by:

```python
from magpie import MagpieServer
m = MagpieServer()
m.run_model('oqmd-dH', ['NaCl'])
```
